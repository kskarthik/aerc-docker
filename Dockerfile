FROM alpine:edge 


RUN echo "http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
		&& apk update && apk upgrade \
		&& apk add aerc man man-pages

ENTRYPOINT ["aerc"]
