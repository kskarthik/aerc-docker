# aerc-docker

aerc email client alpine docker image

This docker image let's you use aerc email client in docker.

* Image size is just 30 MB !

## Install

First, Clone this git respository

`git clone https://gitlab.com/kskarthik/aerc-docker`

cd into the folder 

`$ cd aerc-docker`

Build the docker image

`$ sudo docker build -t aerc .`

After the image is created, Start the container

`$ sudo docker run -it -v aerconf:/root/.config/aerc aerc`

This above command creates a volume named `aerconf` automatically which is used to store aerc configuration. 
So, whenever you stop the container, your data is saved to this volume.

## Removing 

Delete the image 

`$ sudo docker rmi -f aerc` 

Remove the volume 

`$ sudo docker volume rm aerconf`

Done!
